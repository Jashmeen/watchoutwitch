﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BarController : MonoBehaviour {
	
	Image image;
	float fill;
	
	void Start () {
		fill = 1f;
		image = GetComponent<Image>();
		image.fillAmount = 1f;
	}
	
	public void decriment(){
		fill -= 0.05f;
		image.fillAmount = fill;
	}

	public void increment(){
		if(fill+0.1 <= 1f)
			fill += 0.1f;
		image.fillAmount = fill;
	}

	public bool getFill(){
		if(fill > 0)
			return true;
		return false;
	}
}