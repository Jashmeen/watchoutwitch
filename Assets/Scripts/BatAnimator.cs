using UnityEngine;
using System.Collections;

public class BatAnimator : MonoBehaviour {

	public Sprite[] sprites;
	public float framesPerSecond;
	private SpriteRenderer spriteRenderer;
	public GameObject cat;

	// Use this for initialization
	void Start () {
		cat = GameObject.Find("Cat");
		spriteRenderer = renderer as SpriteRenderer;



	}
	
	// Update is called once per frame
	void Update () {
	
		int index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % sprites.Length;
		spriteRenderer.sprite = sprites[ index ];

	}

	void OnBecameInvisible(){
		Destroy(gameObject);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if(coll.gameObject.tag == "Player"){
			Destroy(gameObject);
		}
	}

	public void die(){
		Destroy(gameObject);
	}
	
	public void disable(){
		GetComponent<PolygonCollider2D> ().enabled = false;
	}
}
