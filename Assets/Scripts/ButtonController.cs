﻿using UnityEngine;
using System.Collections;

public class ButtonController : MonoBehaviour {
	
	public Sprite pressedSprite;
	public Sprite unpressedSprite;
	
	private SpriteRenderer spriteRenderer; 
	
	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void ChangeSprite(){
		if (spriteRenderer.sprite == pressedSprite) // if the spriteRenderer sprite = sprite1 then change to sprite2
		{
			spriteRenderer.sprite = unpressedSprite;
		}
		else
		{
			spriteRenderer.sprite = pressedSprite; // otherwise change it back to sprite1
		}
	}
}
