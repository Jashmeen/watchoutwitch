﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
	private float speed;
	private Transform spawnPoint;
	public Transform playerPosition;
	public GameObject Enemy1;
	public GameObject Player;
	public AudioClip deathAudio;
	public AudioClip playerHitAudio;

	private float amplitudeX = 2.0f;
	private float amplitudeY = 1.0f;
	private float omegaX = 1.0f;
	private float omegaY = 5.0f;
	private float index =0;

	private int rand = Random.Range (0,2);
	private int health = 3;
	public int setPosition;


	// Use this for initialization
	void Start () {
		rigidbody2D.velocity = new Vector2 (1, 0);
		spawnPoint = GameObject.Find ("spawnPoint").transform;
		Player = GameObject.FindWithTag ("Player");
		//print (GameObject.FindWithTag ("Player").transform.position.x + .2);
		//print (GameObject.FindWithTag ("Enemy").transform.position.x);
	}
	public void TakeDamage(int damage){
		health -= damage;
		if(health  <= 0){
			Destroy (gameObject);
		}
	}
	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.name =="fire(Clone)") {
			Debug.Log ("hit fire");
			AudioSource.PlayClipAtPoint(deathAudio,transform.position);
		}
		else if (coll.gameObject.name == "ice(Clone)") {
			Debug.Log ("hit ice");
			AudioSource.PlayClipAtPoint(deathAudio,transform.position);
		}
		else if (coll.gameObject.name == "magic(Clone)") {
			Debug.Log ("hit magic");
			AudioSource.PlayClipAtPoint(deathAudio,transform.position);
		}
		else if (coll.gameObject.name == "Witch"){
			Debug.Log ("Hit player");
			AudioSource.PlayClipAtPoint (playerHitAudio, transform.position);
		}
	}
	// Update is called once per frame
	void Update () {
		if(setPosition ==1)
		{
			speed = 0.1f;
			//old code
//			playerPosition = GameObject.FindWithTag("Player").transform;
//			transform.position = Vector3.MoveTowards(transform.position, playerPosition.position, speed);
//			//Vector2 vectorBetweenEnemyPlayer = GameObject.FindWithTag ("Player").transform.position - GameObject.FindWithTag ("Enemy").transform.position;
//			if(GameObject.FindWithTag ("Enemy").transform.position.x < GameObject.FindWithTag ("Player").transform.position.x + 3){
//				//print ("at position");
//				rigidbody2D.velocity  = new Vector2(speed, GameObject.FindWithTag ("Enemy").transform.position.y);
//			}
			if(transform.position.x > Player.transform.position.x + 0.1f){
				transform.position = Vector3.MoveTowards (transform.position, Player.transform.position, speed);
			}
		}
		else if(setPosition ==2)
		{
			//sinusoidal movement
			//index += Time.deltaTime;
			//float x = transform.position.x * Mathf.Cos (omegaX*index);
			//float y = Mathf.Abs (amplitudeY*Mathf.Sin (omegaY*index));

			//diagonal movement that bounces on the edges of the screen
			speed = 3f;

			Vector3 diagonal;
			Vector3 newPosition = transform.position;
			Camera mainCamera = Camera.main;
			Vector3 cameraPosition = mainCamera.transform.position;
			
			float xDist = mainCamera.aspect * mainCamera.orthographicSize;
			float xMax = cameraPosition.x + xDist;
			float xMin = cameraPosition.x - xDist;
			float yMax = mainCamera.orthographicSize;

			if(rand ==0)
			{
				diagonal = Vector3.left + Vector3.up;
				diagonal = diagonal.normalized;


			}
			else if(rand ==1)
			{
				diagonal = Vector3.left + Vector3.down;
				diagonal = diagonal.normalized;
			}
			else
			{
				return;
			}

			if(newPosition.y < -yMax)
			{
				newPosition.y = Mathf.Clamp (newPosition.y, -yMax, yMax);
				rand = 0;
				transform.position=newPosition;
				return;
			}
			else if(newPosition.y >yMax)
			{
				newPosition.y = Mathf.Clamp (newPosition.y, -yMax, yMax);
				rand = 1;
				transform.position=newPosition;
				return;
			}


			transform.position += diagonal *speed * Time.deltaTime;



		}
		else if(setPosition ==3)
		{
			//horizontal movement
			speed = 2.0f;
			transform.position+=Vector3.left *speed * Time.deltaTime;
		}

	}

	public void die(){
		Destroy(gameObject);
	}

	public void disable(){
		GetComponent<PolygonCollider2D> ().enabled = false;
	}
}

	
