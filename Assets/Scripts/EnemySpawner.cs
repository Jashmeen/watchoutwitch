﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
	GameObject enemy;
	GameObject power;
	GameObject barrier;
	public GameObject spider;
	Transform spawn1;
	Transform spawn2;
	Transform spawn3;
	public GameObject Enemy1, Enemy2, Enemy3;
	public GameObject fire, ice, magic;
	public GameObject obstacle;
	public float spawnArea = 1f;
	public int rate = 5;
	private float timer = 0;
	private float waveTimer = 10;
	private int timeBetweenSpiders;
	private int secondWave;
	private float difficultyMult = 6f;
	private float spawnTime = 1;
	private GameObject[] numberOfEnemies;

	// Use this for initialization
	void Start () {
		timeBetweenSpiders = Random.Range (10, 26);
		secondWave = Random.Range (10, 20);
		InvokeRepeating ("spawnEnemies", difficultyMult, difficultyMult);
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		waveTimer -= Time.deltaTime;
		numberOfEnemies = GameObject.FindGameObjectsWithTag ("Enemy");
		//Debug.Log ("timer: " + timer);
		if (timer > timeBetweenSpiders) {
			GameObject spiderEnemy = Instantiate (spider) as GameObject;
			timeBetweenSpiders = Random.Range (10, 26);
			timer = 0;
		}
		if(numberOfEnemies.Length < 10){
			if(waveTimer < 0){

				//spawnEnemies ();
				InvokeRepeating ("spawnEnemies", difficultyMult, difficultyMult);
				if(difficultyMult > 3)
					difficultyMult -= 1;
				else if(difficultyMult == 4){
					difficultyMult = 4;
				}
			//spawnEnemies ();
			//spawnEnemies ();
				Debug.Log ("Timer reached 0");
				waveTimer = 10;
			}
		}
	}
	int j = 0;
	/*void spawnObstacles(){
		GameObject[] obstacleSpawnPoints = GameObject.FindGameObjectsWithTag ("obstacleSpawn");
		foreach(GameObject obstacleSpawn in obstacleSpawnPoints){
			barrier = Instantiate (obstacle, new Vector3(obstacleSpawn.transform.position.x + Random.Range(-spawnArea,spawnArea),
			                                             obstacleSpawn.transform.position.y + Random.Range(-spawnArea,spawnArea),
			                                             0)
			                       
			                       
			                       , obstacleSpawn.transform.rotation) as GameObject;
		}
	}*/
	void spawnEnemies(){
		GameObject[] enemySpawns = GameObject.FindGameObjectsWithTag ("EnemySpawnPoint");

		foreach(GameObject spawnPoint in enemySpawns){
			int noOfEnemies = Random.Range (0,3);
			for(int i = 0; i < noOfEnemies; i++){
				j++;
				int enemyType = Random.Range (0,3);
				if(enemyType == 0){
					GameObject enemy;
					enemy = Instantiate (Enemy1, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
					                                        spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
					                                        0)
					                     
					                     
					                     , spawnPoint.transform.rotation) as GameObject;
					int chance = Random.Range (1,101);
					if(chance <= rate){
						int which = Random.Range(0,3);
						switch(which){
							case 0:
								power = Instantiate(fire, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
								                                      spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
								                                      0)
								                    
								                    
								                    , spawnPoint.transform.rotation) as GameObject;
								break;
							case 1:
								power = Instantiate(ice, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                     spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                     0)
							                    
							                    
							                    , spawnPoint.transform.rotation) as GameObject;
								break;
							case 2:
								power =  Instantiate(magic, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                      spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                      0)
							                     
							                     
							                     , spawnPoint.transform.rotation) as GameObject;
								break;
							default:
								break;
						}
					}
					/*int obstacleChance = Random.Range (1,10);
					if(obstacleChance <= rate){
						Debug.Log ("in obstacle chance");
						barrier = Instantiate (obstacle, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
						                                             spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
						                                             0)
						                       
						                       
						                       , spawnPoint.transform.rotation) as GameObject;
					}*/
				}
				else if(enemyType == 1){
					GameObject enemy;
					enemy = Instantiate (Enemy2, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
					                                         spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
					                                         0)
					                     
					                     
					                     , spawnPoint.transform.rotation) as GameObject;
					int chance = Random.Range (1,101);
					if(chance <= rate){
						int which = Random.Range(0,3);
						switch(which){
						case 0:
							power = Instantiate(fire, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                      spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                      0)
							                    
							                    
							                    , spawnPoint.transform.rotation) as GameObject;
							break;
						case 1:
							power = Instantiate(ice, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                     spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                     0)
							                    
							                    
							                    , spawnPoint.transform.rotation) as GameObject;
							break;
						case 2:
							power =  Instantiate(magic, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                        spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                        0)
							                     
							                     
							                     , spawnPoint.transform.rotation) as GameObject;
							break;
						default:
							break;
						}
					}
				}
				else if(enemyType == 2){
					GameObject enemy;
					enemy = Instantiate (Enemy3, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
					                                         spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
					                                         0)
					                     
					                     
					                     , spawnPoint.transform.rotation) as GameObject;
					int chance = Random.Range (1,101);
					if(chance <= rate){
						int which = Random.Range(0,3);
						switch(which){
						case 0:
							power = Instantiate(fire, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                      spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                      0)
							                    
							                    
							                    , spawnPoint.transform.rotation) as GameObject;
							break;
						case 1:
							power = Instantiate(ice, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                     spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                     0)
							                    
							                    
							                    , spawnPoint.transform.rotation) as GameObject;
							break;
						case 2:
							power =  Instantiate(magic, new Vector3(spawnPoint.transform.position.x + Random.Range(-spawnArea,spawnArea),
							                                        spawnPoint.transform.position.y + Random.Range(-spawnArea,spawnArea),
							                                        0)
							                     
							                     
							                     , spawnPoint.transform.rotation) as GameObject;
							break;
						default:
							break;
						}
					}
				}


			}
		}

	}
}
