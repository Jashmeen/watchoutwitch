﻿using UnityEngine;
using System.Collections;

public class HighScore: MonoBehaviour 
{
	public bool levelComplete;
	public string highscorePos;
	public int score;
	public int temp;
	
	void Start () 
	{
		score=0;
	}
	
	void OnLevelLoad () 
	{
		levelComplete=true;
		score=10000; //values from your scoring logic
		for(int i=1; i<=5; i++) //for top 5 highscores
		{
			if(PlayerPrefs.GetInt("highscorePos"+i)<score)    
			{
				temp=PlayerPrefs.GetInt("highscorePos"+i);     
				PlayerPrefs.SetInt("highscorePos"+i,score);    
				if(i<5)                                        
				{
					int j=i+1;
					PlayerPrefs.SetInt("highscorePos"+j,temp);    
				}
			}
		}
	}
	
	void OnGUI() 
	{
		if(levelComplete)
		{
			for(int i=1; i<=5; i++)
			{
				GUI.Box(new Rect(100, 75*i, 150, 50), "Pos "+i+". "+PlayerPrefs.GetInt("highscorePos"+i));
			}
		}
	}
}