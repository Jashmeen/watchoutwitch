﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeaderBoard : MonoBehaviour {

	public Text scoreText;
	public Text scoreText2;
	public Text scoreText3;

	// Use this for initialization
	void Start () {
		scoreText.text = "High Score: " + PlayerPrefs.GetInt ("High Score");
		scoreText2.text = "This Run: " + PlayerPrefs.GetInt ("Current Score");

		if (PlayerPrefs.GetInt ("Current Score") <= 500)
			scoreText3.text = "Apprentice Witch";
		else if(PlayerPrefs.GetInt ("Current Score") <= 1500)
			scoreText3.text = "Witch-In-Training";
		else if(PlayerPrefs.GetInt ("Current Score") <= 2500)
			scoreText3.text = "Witch Adept";
		else if(PlayerPrefs.GetInt ("Current Score") <= 5000)
			scoreText3.text = "Master Witch";
		else if (PlayerPrefs.GetInt ("Current Score") >5000)
			scoreText3.text = "Cat Lover";
		//OnGUI ();
	}

	void Awake(){


		}
	
	// Update is called once per frame
	void Update () {
		//scoreText.text = "High Score: " + PlayerPrefs.GetInt ("High Score");
	}

	//void OnGUI() 
	//{

	//		for(int i=2; i<=5; i++)
	//		{
	//			GUI.Box(new Rect(280, 75*i/1.3f, 150, 50), i+")  "+PlayerPrefs.GetInt("High Score"+i));
	//		}
		
	//}
}
