﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	
	public float currenty; 
	public float speed = 1.0f;
	public float glowTime = 1f;
	public AudioClip playerHitAudio;
	public AudioClip powerUpAudio;
	private Vector3 moveDirection;
	private float xMax;
	float spritex;
	float witchstart;
	float lerptime = 1f;
	float currentLerpTime;
	public GameObject cat;
	public Image red, blue, purple;
	private ParticleSystem glow;
	private float glowTimer;
	
	
	// Use this for initialization
	void Start () {
		glow = GetComponent<ParticleSystem> ();
		glow.enableEmission = false;
		spritex = gameObject.renderer.bounds.size.x * 100;
		witchstart = Camera.main.transform.position.x + spritex;
		transform.position = Camera.main.ScreenToWorldPoint(new Vector3(witchstart, Screen.height/2, 10f));
		moveDirection = Vector3.right;
		bool supportsMultiTouch = Input.multiTouchEnabled;
		print("MultiTouchSupport : " + supportsMultiTouch);

	}
	
	// Update is called once per frame
	void Update () {	
		//handle glowing if you just picked up a powerup
		glowTimer += Time.deltaTime;
		if (glowTimer >= glowTime) {
			glow.enableEmission = false;
		}

		Vector3 pos1 = transform.position;


		currentLerpTime += Time.deltaTime;
		if(currentLerpTime > lerptime){
			currentLerpTime = lerptime;
		}

		float perc = currentLerpTime / lerptime;

		for (int i=0; i< Input.touchCount-1; i++) {
			if (Input.touchCount > 0 && Input.GetTouch (i).position.x < Screen.width / 2) {
				Touch touch = Input.GetTouch (0);
				if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
					// If the finger is on the screen, move the object smoothly to the touch position
					Vector3 touchPosition = Camera.main.ScreenToWorldPoint (new Vector3 (witchstart, touch.position.y, 10));                
					transform.position = Vector3.MoveTowards (transform.position, touchPosition, speed);


				}
			}
		}
		if (Input.GetMouseButton(0) && Input.mousePosition.x < Screen.width/2){
			Vector3 mouseDeltaPosition = Camera.main.ScreenToWorldPoint(new Vector3(witchstart, Input.mousePosition.y, 10));
			transform.position = Vector3.MoveTowards(transform.position, mouseDeltaPosition, speed);
		}
		
	}
	
	void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("Enemy")){
			Debug.Log ("Hit Player");
			cat.GetComponent<catAnimator>().speedUp();
			GetComponent<Animator>().SetTrigger("isHit");
			audio.PlayOneShot(playerHitAudio);
			Destroy(other.gameObject);
		}
		else if(other.CompareTag ("Obstacle")){
			Debug.Log ("Hit Player OBSTACLE");
			cat.GetComponent<catAnimator>().speedUp();
			GetComponent<Animator>().SetTrigger("isHit");
			audio.PlayOneShot(playerHitAudio);
			//Destroy(other.gameObject);
		}
		else if(other.CompareTag("powerUp")){
			glowTimer = 0;
			if (other.name.Equals("FirePower(Clone)")){
				red.GetComponent<BarController>().increment();
				red.GetComponent<BarController>().increment();
				red.GetComponent<BarController>().increment();
				glow.startColor = new Color32(255, 231, 195, 255);
				glow.enableEmission = true;
			}
			else if(other.name.Equals("IcePower(Clone)")){
				blue.GetComponent<BarController>().increment();
				blue.GetComponent<BarController>().increment();
				blue.GetComponent<BarController>().increment();
				glow.startColor = new Color32(195, 243, 255, 255);
				glow.enableEmission = true;
			}
			else if(other.name.Equals("MagicPower(Clone)")){
				purple.GetComponent<BarController>().increment();
				purple.GetComponent<BarController>().increment();
				purple.GetComponent<BarController>().increment();
				glow.startColor = new Color32(240, 216, 255, 255);
				glow.enableEmission = true;
			}
			Debug.Log ("hit power up"); 
			audio.PlayOneShot (powerUpAudio);
			Destroy(other.gameObject);
		}
	}
}