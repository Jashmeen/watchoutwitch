using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProjectileProperties : MonoBehaviour {
	public AudioClip enemyDeathAudio;

	public GameObject UI;
	public GameObject cat;
	public float bulletSpeed = 10;
	private bool isMatch = false;
	private int damage = 1;

	// Use this for initialization
	void Start () {
		cat = GameObject.Find("Cat");
		UI = GameObject.Find("UI");
		rigidbody2D.velocity = Vector2.right * bulletSpeed;
		//transform.position = Vector3.MoveTowards(transform.position, Input.mousePosition, bulletSpeed);
	}
	
	// Update is called once per frame
	void Update () {
//		if(transform.position.x > (Camera.main.orthographicSize * Camera.main.aspect)){
//			Destroy (gameObject);
//		}
	}
	void OnCollisionEnter2D(Collision2D coll) {
				Debug.Log ("Hit something");
				if (coll.gameObject.tag == "Enemy") {
						cat.GetComponent<catAnimator> ().slowDown ();
						if (coll.gameObject.name == "Enemy(Clone)" && gameObject.name == "magic(Clone)") {
								UI.GetComponent<UIController> ().incrPurple ();
								UI.GetComponent<UIController> ().matchHit ();
								isMatch = true;
						}

						if (coll.gameObject.name == "BlueEnemy(Clone)" && gameObject.name == "ice(Clone)") {

								UI.GetComponent<UIController> ().incrBlue ();
								UI.GetComponent<UIController> ().matchHit ();
								isMatch = true;
						}
						
						if (coll.gameObject.name == "AkaEnemy(Clone)" && gameObject.name == "fire(Clone)") {
								UI.GetComponent<UIController> ().incrRed ();
								UI.GetComponent<UIController> ().matchHit ();
								isMatch = true;
						}
			if (coll.gameObject.name == "Spider(Clone") {
				UI.GetComponent<UIController> ().matchHit ();
				isMatch = true;
			}
						if (isMatch == false) {
								UI.GetComponent<UIController> ().notHit ();
						}
						audio.PlayOneShot (enemyDeathAudio);
						//Destroy (coll.gameObject);
						coll.gameObject.GetComponent<Animator>().SetTrigger("dead");
						Destroy (gameObject);
								
						
		
				}
		}

	void OnBecameInvisible(){
		Destroy(gameObject);
	}
}
