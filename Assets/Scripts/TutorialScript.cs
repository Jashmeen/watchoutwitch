﻿using UnityEngine;
using System.Collections;

public class TutorialScript : MonoBehaviour {

	private bool one, two, three, four;
	public GameObject witchText, catText, buttonText, batText, barText, PlayGame, RedEnemy;

	// Use this for initialization
	void Start () {
		//Time.timeScale = 0;
		one = true;
		two = false;
		three = false;
		four = false;
		batText.SetActive (false);
		barText.SetActive (false);
		RedEnemy.SetActive (false);
		PlayGame.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {

		//on first touch
		if (one && ((Input.touchCount > 0 && Input.GetTouch (0).Equals ( TouchPhase.Began)) || Input.GetMouseButtonDown(0))) {
			witchText.SetActive(false);
			catText.SetActive(false);
			buttonText.SetActive(false);
			batText.SetActive(true);
			barText.SetActive(true);
			RedEnemy.SetActive(true);
			one = false;
			two = true;
		}
		else if (two && ((Input.touchCount > 0 && Input.GetTouch (0).Equals ( TouchPhase.Began)) || Input.GetMouseButtonDown(0))) {
			batText.SetActive(false);
			barText.SetActive(false);
			two = false;
			three = true;
		}
		else if (RedEnemy == null && three) {
			PlayGame.SetActive(true);
			three = false;
			four = true;
		}
		else if (four && ((Input.touchCount > 0 && Input.GetTouch (0).Equals ( TouchPhase.Began)) || Input.GetMouseButtonDown(0))) {
			Application.LoadLevel("jasmine_testscene");
		}
	}
}
