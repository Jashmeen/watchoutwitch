﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {

	//idk how to do enums, its probably easy but w/e
	//red is 1, blue is 2, purple is 3
	public int colorType = 1;

	public int score;
	public int high;
	public Text scoreText;

	public GameObject fireButton;
	public GameObject blueButton;
	public GameObject redButton;
	public GameObject purpleButton;

	private GameObject currentButton = null;
	private GameObject shootButton;
	private GameObject player;
	private GameObject runtimeRed;
	private Vector3 redMove;
	private GameObject runtimeBlue;
	private Vector3 blueMove;
	private GameObject runtimePurple;
	private Vector3 purpleMove;
	private Vector3 shootMove;
	
	public Image redBar;
	public Image blueBar;
	public Image purpleBar;

	public bool tutorial;

	private Animator witchAnim;

	private float xMax;

	public GameObject canvas;

	// Use this for initialization
	void Start () {

		score = 0;
		high = 0;

		updateScore ();

		//redBar = GameObject.Find("redBar");
		//blueBar = GameObject.Find("blueBar");
		//purpleBar = GameObject.Find("purpleBar");

		Camera mainCamera = Camera.main;
		Vector3 cameraPosition = mainCamera.transform.position;

		bool supportsMultiTouch = Input.multiTouchEnabled;
		print("MultiTouchSupport : " + supportsMultiTouch);

		//get x and y positions of the edge of the camera
		float xDist = mainCamera.aspect * mainCamera.orthographicSize;
		xMax = cameraPosition.x + xDist;
		float yDist = mainCamera.orthographicSize;
		float yMax = cameraPosition.y + yDist;

		//place buttons where they should go
		float fireButtonxPos = xMax - (fireButton.GetComponent<Renderer>().bounds.size.x - 1.3f);
		float fireButtonyPos = yMax - (fireButton.GetComponent<Renderer>().bounds.size.y + 2.8f);
		Vector3 firePos = new Vector3(fireButtonxPos, fireButtonyPos, 0);
		Instantiate(fireButton, firePos, Quaternion.identity);

		float blueButtonxPos = xMax - (blueButton.GetComponent<Renderer>().bounds.size.x + 0.25f);
		float blueButtonyPos = yMax - (blueButton.GetComponent<Renderer>().bounds.size.y + 2.4f);
		Vector3 bluePos = new Vector3(blueButtonxPos, blueButtonyPos, 0);
		Instantiate(blueButton, bluePos, Quaternion.identity);

		float redButtonxPos = xMax - (redButton.GetComponent<Renderer>().bounds.size.x + 1.45f);
		float redButtonyPos = yMax - (redButton.GetComponent<Renderer>().bounds.size.y + 3.25f);
		Vector3 redPos = new Vector3(redButtonxPos, redButtonyPos, 0);
		Instantiate(redButton, redPos, Quaternion.identity);

		float purpleButtonxPos = xMax - (purpleButton.GetComponent<Renderer>().bounds.size.x + 1.5f);
		float purpleButtonyPos = yMax - (purpleButton.GetComponent<Renderer>().bounds.size.y + 4.5f);
		Vector3 purplePos = new Vector3(purpleButtonxPos, purpleButtonyPos, 0);
		Instantiate(purpleButton, purplePos, Quaternion.identity);

		shootButton = GameObject.Find("ShootButton(Clone)");

		player = GameObject.Find ("Witch");
		witchAnim = player.GetComponent<Animator>();


		shootMove = shootButton.transform.position;
		player = GameObject.FindWithTag("Player");
		runtimeRed = GameObject.Find ("redButton(Clone)");
		redMove = runtimeRed.transform.position;
		runtimeBlue = GameObject.Find ("blueButton(Clone)");
		blueMove = runtimeBlue.transform.position;
		runtimePurple = GameObject.Find ("purpleButton(Clone)");
		purpleMove = runtimePurple.transform.position;

		runtimeRed.transform.parent = canvas.transform;
		shootButton.transform.parent = canvas.transform;
		runtimeBlue.transform.parent = canvas.transform;
		runtimePurple.transform.parent = canvas.transform;



	}
	
	// Update is called once per frame
	void Update () {

		//use this for debugging if needed
//		if (Input.anyKeyDown) {
//			redBar.GetComponent<BarController>().increment();
//		}

		//move the buttons with respect to the camera

		if (!tutorial) {
			shootMove.x += Time.deltaTime * 1f;
			redMove.x += Time.deltaTime * 1f;
			blueMove.x += Time.deltaTime * 1f;
			purpleMove.x += Time.deltaTime * 1f;
			shootButton.transform.position = shootMove;
			runtimeRed.transform.position = redMove;
			runtimeBlue.transform.position = blueMove;
			runtimePurple.transform.position = purpleMove;
		}


		bool changeColor = false;
		bool isShooting = false;
		bool touching = false;
		GameObject dostuffwith = null;


		//touch stuff
		int nbTouches = Input.touchCount;
		if(nbTouches > 0){
			for (int i = 0; i < nbTouches-1; i++){
				Touch touch = Input.GetTouch(1);
				if(touch.position.x > Screen.width/2 && touch.phase == TouchPhase.Began){
					touching = true;
					Collider2D hitCollider = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10)));
					if(hitCollider && hitCollider.gameObject.tag.Equals("colorButton")){
						dostuffwith = hitCollider.gameObject;
						currentButton = dostuffwith;
						dostuffwith.GetComponent<ButtonController>().ChangeSprite();
						if(dostuffwith.name.Equals("redButton(Clone)")){
							colorType = 1;
							changeColor = true;
						}
						if(dostuffwith.name.Equals("blueButton(Clone)")){
							colorType = 2;
							changeColor = true;
						}
						if(dostuffwith.name.Equals("purpleButton(Clone)")){
							colorType = 3;
							changeColor = true;
						}
					}
					else if(hitCollider.gameObject.name.Equals("ShootButton(Clone)")){
						bool canShoot = false;
						switch(colorType){
						case 1: 
							canShoot = redBar.GetComponent<BarController>().getFill();
							break;
						case 2: 
							canShoot = blueBar.GetComponent<BarController>().getFill();
							break;
						case 3: 
							canShoot = purpleBar.GetComponent<BarController>().getFill();
							break;
						default:
							break;
						}
						if(canShoot){
							witchAnim.SetTrigger("isShooting");
							shootButton.GetComponent<shootButtonController>().pushDown();
							isShooting = true;
							Quaternion rot = Quaternion.Euler(new Vector3(0,-40,0));
							switch(colorType){
							case 1: 
								redBar.GetComponent<BarController>().decriment();
								Instantiate (Resources.Load ("fire"), player.transform.position, rot);
								break;
							case 2: 
								blueBar.GetComponent<BarController>().decriment();
								Instantiate (Resources.Load ("ice"), player.transform.position, rot);
								break;
							case 3: 
								purpleBar.GetComponent<BarController>().decriment();
								Instantiate (Resources.Load ("magic"), player.transform.position, rot);
								break;
							default:
								break;
							}
						}
					}
				}
			}
		}

		//mouse stuff
		if(Input.GetMouseButtonDown(0) && Input.mousePosition.x > Screen.width/2){
			Vector2 mousePostiton = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Collider2D hitCollider = Physics2D.OverlapPoint(mousePostiton);
			if(hitCollider){
				//if you are pressing a color change button
				if(hitCollider.gameObject.tag.Equals("colorButton")){
					dostuffwith = hitCollider.gameObject;
					currentButton = dostuffwith;
					dostuffwith.GetComponent<ButtonController>().ChangeSprite();
					if(dostuffwith.name.Equals("redButton(Clone)")){
						colorType = 1;
						changeColor = true;
					}
					if(dostuffwith.name.Equals("blueButton(Clone)")){
						colorType = 2;
						changeColor = true;
					}
					if(dostuffwith.name.Equals("purpleButton(Clone)")){
						colorType = 3;
						changeColor = true;
					}
				}
				//else if you are pressing the fire button
				else if(hitCollider.gameObject.name.Equals("ShootButton(Clone)")){
					bool canShoot = false;
					switch(colorType){
					case 1: 
						canShoot = redBar.GetComponent<BarController>().getFill();
						break;
					case 2: 
						canShoot = blueBar.GetComponent<BarController>().getFill();
						break;
					case 3: 
						canShoot = purpleBar.GetComponent<BarController>().getFill();
						break;
					default:
						break;
					}
					if(canShoot){
						witchAnim.SetBool("isShooting", true);
						shootButton.GetComponent<shootButtonController>().pushDown();
						isShooting = true;
						Quaternion rot = Quaternion.Euler(new Vector3(0,-40,0));
						//Instantiate (Resources.Load ("fire"), player.transform.position, rot);
						switch(colorType){
							case 1: 
								redBar.GetComponent<BarController>().decriment();
								Instantiate (Resources.Load ("fire"), player.transform.position, rot);
								break;
							case 2: 
								blueBar.GetComponent<BarController>().decriment();
								Instantiate (Resources.Load ("ice"), player.transform.position, rot);
								break;
							case 3: 
								purpleBar.GetComponent<BarController>().decriment();
								Instantiate (Resources.Load ("magic"), player.transform.position, rot);
								break;
							default:
								break;
						}
					}
				}
			}
		}

		//only have the pressed down sprite while the user is holding down the mouse/touch
		if((Input.GetMouseButtonUp(0) || touching) && currentButton != null && currentButton.tag.Equals("colorButton")){
			currentButton.GetComponent<ButtonController>().ChangeSprite();
			currentButton = null;
			touching = false;
		}
		if ((Input.GetMouseButtonUp(0) || touching) && isShooting){ //letting off the shootbutton
			shootButton.GetComponent<shootButtonController>().pushDown();
			touching = false;
		}
		//end button code



		if(changeColor){
			switch(colorType){
				case 1: 
					shootButton.GetComponent<shootButtonController>().ChangeSprite(1);
					break;
				case 2:
					shootButton.GetComponent<shootButtonController>().ChangeSprite(2);
					break;
				case 3:
					shootButton.GetComponent<shootButtonController>().ChangeSprite(3);
					break;
				default:
					break;
			}
		}

	}

	public void incrPurple(){
		purpleBar.GetComponent<BarController>().increment();
	}

	public void incrBlue(){
		blueBar.GetComponent<BarController>().increment();
	}

	public void incrRed(){
		redBar.GetComponent<BarController>().increment();
	}

	//same color hit enemy
	public void matchHit()
	{
		score += 200;
		updateScore ();
	}

	//different color hits enemy
	public void notHit()
	{
		score += 100;
		updateScore ();
	}

	private void updateScore()
	{

		scoreText.text = "Score: " + score;
		if (high < score) {
			high = score;
			PlayerPrefs.SetInt ("Current Score", high);
			if(high >=PlayerPrefs.GetInt ("High Score"))
				PlayerPrefs.SetInt("High Score", high);
				}
	}
	public int getScore()
	{
		return high;
	}
}
