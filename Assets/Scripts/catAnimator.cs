﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class catAnimator : MonoBehaviour {
	
	public Sprite[] sprites;
	public float framesPerSecond;
	private SpriteRenderer spriteRenderer;

	private Vector3 target;
	private Vector3 movement;
	public float catSpeed;
	public float slowDownSpeed;
	public float speedUpSpeed;

	public int score1;

	public float distance;
	private float startTime;
	private float journeyLength;

	private float xMin;
	private float xMax;

	private bool moveForward;
	private bool moveBack;
	
	// Use this for initialization
	void Start () {
		spriteRenderer = renderer as SpriteRenderer;
		movement = transform.position;
		catSpeed = 0.10f;
		target = transform.position;
		moveForward = false;
		moveBack = false;
	}
	
	// Update is called once per frame
	void Update () {
		int index = (int)(Time.timeSinceLevelLoad * framesPerSecond);
		index = index % sprites.Length;
		spriteRenderer.sprite = sprites[ index ];
		movement = transform.position;
		if (journeyLength != 0) {
			float frac = (Time.time - startTime) * catSpeed / journeyLength;
			transform.position = Vector3.Lerp (transform.position, target, frac);
			if( (moveForward && transform.position.x >= target.x) || (moveBack && transform.position.x <= target.x) ){
				journeyLength = 0;
				moveForward = false;
				moveBack = false;
			}
		}
		//Movement
//		movement.x += Time.deltaTime + 0.0001f;
//		transform.position = movement;

		checkBounds ();
	}

	private void checkBounds()
	{
		Vector3 newPosition = transform.position;
		Camera mainCamera = Camera.main;
		Vector3 cameraPosition = mainCamera.transform.position;

		float xDist = mainCamera.aspect * mainCamera.orthographicSize;
		xMax = cameraPosition.x + xDist;
		xMin = cameraPosition.x - xDist;

		if(newPosition.x > xMax)
		{
			Application.LoadLevel ("Leaderboard");
		}

//		if(newPosition.x < xMin){
//			Application.LoadLevel ("Win");
//		}
	}

	public void speedUp(){
		Vector3 newPos = new Vector3(movement.x + 3.5f, movement.y, movement.z);
		target = newPos;
		startTime = Time.time;
		journeyLength = Vector3.Distance (transform.position, target);
		moveForward = true;
	}

	public void slowDown(){
		Debug.Log (movement.x + " " + xMin);
		if (movement.x < xMin + 1) {
			return;
		}
		Vector3 newPos = new Vector3(movement.x - 0.0001f, movement.y, movement.z);
		target = newPos;
		startTime = Time.time;
		journeyLength = Vector3.Distance (transform.position, target);
		moveBack = true;

	}
}