﻿using UnityEngine;
using System.Collections;

public class enemyProjectile : MonoBehaviour {
	public float speed;


	// Use this for initialization
	void Start () {
		rigidbody2D.velocity = Vector2.right * speed;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnBecameInvisible(){
		Destroy(gameObject);
	}
}
