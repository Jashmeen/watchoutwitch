﻿using UnityEngine;
using System.Collections;

public class obstacleSpawner : MonoBehaviour {
	public GameObject Obstacle;
	GameObject barrier;
	bool spawnFlag;
	public float spawnArea = .5f;
	static float timer = 10f;
	// Use this for initialization
	void Start () {
		//Obstacle = GameObject.FindGameObjectWithTag ("Obstacle");
		spawnFlag = true;
	}
	
	// Update is called once per frame
	void Update () {
		timer -= Time.deltaTime;
		Debug.Log ("time: " + timer);
		if(timer < 0){
			//timer = 3;
			Debug.Log ("this is the timer: " + timer);
			Debug.Log ("in update");
			if(spawnFlag == true){
				spawnFlag = false;
				if(spawnFlag == false){
					spawnObstacle ();
			}
			}

			spawnFlag = true;
		}
		if(timer < 0){

			timer = 3;
			Debug.Log ("timer: " + timer);
		}
	}
	void spawnObstacle(){

		GameObject obstacleSpawnPoints1 = GameObject.FindGameObjectWithTag ("obstacleSpawn1");
		GameObject obstacleSpanwPoints2 = GameObject.FindGameObjectWithTag ("obstacleSpawn2");
		//foreach(GameObject obstacleSpawn in obstacleSpawnPoints){
		int spawn = Random.Range (0, 1);
		if(spawn == 0){
			barrier = Instantiate (Obstacle, new Vector3(obstacleSpawnPoints1.transform.position.x + Random.Range(-spawnArea,spawnArea),
			                                         obstacleSpawnPoints1.transform.position.y + Random.Range(-spawnArea,spawnArea),
			                                         0)
			                     
			                     
			                     , obstacleSpawnPoints1.transform.rotation) as GameObject;
			Debug.Log ("spawning1");
			//}
		}
		else{
			barrier = Instantiate (Obstacle, new Vector3(obstacleSpanwPoints2.transform.position.x + Random.Range(-spawnArea,spawnArea),
			                                             obstacleSpanwPoints2.transform.position.y + Random.Range(-spawnArea,spawnArea),
			                                             0)
			                       
			                       
			                       , obstacleSpanwPoints2.transform.rotation) as GameObject;
			Debug.Log ("spawning2");
		}
	}
}
