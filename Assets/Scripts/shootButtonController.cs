﻿using UnityEngine;
using System.Collections;

public class shootButtonController : MonoBehaviour {
	public AudioClip fireSpell;
	public AudioClip iceSpell;
	public AudioClip purpSpell;

	public Sprite redPressed;
	public Sprite redUnpressed;

	public Sprite bluePressed;
	public Sprite blueUnpressed;

	public Sprite purplePressed;
	public Sprite purpleUnpressed;

	private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeSprite(int which){
		spriteRenderer = GetComponent<SpriteRenderer>();
		switch(which){
		case 1: 
			spriteRenderer.sprite = redUnpressed;
			break;
		case 2:
			spriteRenderer.sprite = blueUnpressed;
			break;
		case 3:
			spriteRenderer.sprite = purpleUnpressed;
			break;
		default:
			break;
		}
	}

	public void pushDown(){
		if(spriteRenderer.sprite == redUnpressed){
			spriteRenderer.sprite = redPressed;
			Debug.Log ("Pressed fire spell");
			//audio.PlayOneShot(fireSpell);
		}
		else if(spriteRenderer.sprite == redPressed){
			spriteRenderer.sprite = redUnpressed;
		}

		if(spriteRenderer.sprite == blueUnpressed){
			spriteRenderer.sprite = bluePressed;
			Debug.Log ("Pressed ice spell");
			audio.PlayOneShot(iceSpell);
		}
		else if(spriteRenderer.sprite == bluePressed){
			spriteRenderer.sprite = blueUnpressed;
		}

		if(spriteRenderer.sprite == purpleUnpressed){
			spriteRenderer.sprite = purplePressed;
			audio.PlayOneShot(purpSpell);
		}
		else if(spriteRenderer.sprite == purplePressed){
			spriteRenderer.sprite = purpleUnpressed;
		}
	}
}
