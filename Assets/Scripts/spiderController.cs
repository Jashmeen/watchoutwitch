﻿using UnityEngine;
using System.Collections;

public class spiderController : MonoBehaviour {
	public GameObject web;
	public float speed = 1f;
	public int changeDirChance = 1;
	public float secondsBetweenShots;
	private float yMax;
	private float yMin;
	private float timer;


	// Use this for initialization
	void Start () {
		timer = 0;
		Camera mainCamera = Camera.main;
		Vector3 cameraPosition = mainCamera.transform.position;
		
		float xDist = mainCamera.aspect * mainCamera.orthographicSize;
		float xMax = cameraPosition.x + xDist;
		float xMin = cameraPosition.x - xDist;

		float yDist = mainCamera.orthographicSize;
		yMax = cameraPosition.y + yDist - 0.8f;
		yMin = cameraPosition.y - yDist + 3f;

		transform.position = new Vector3 (xMax-0.8f, transform.position.y, transform.position.z);
		GameObject webShoot = Instantiate (web) as GameObject;
		webShoot.transform.position = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//shoot
		timer += Time.deltaTime;
		float offset = Random.Range (secondsBetweenShots, secondsBetweenShots + 5);
		if (timer > offset) {
			GameObject webShoot = Instantiate (web) as GameObject;
			webShoot.transform.position = transform.position;
			timer = 0;
		}

		Vector3 pos = transform.position;
		pos.y += speed * Time.deltaTime;
		pos.x += 1f * Time.deltaTime;
		transform.position = pos;

		if (pos.y <= yMin) {
			speed = Mathf.Abs (speed);
		} else if (pos.y > yMax) {
			speed = -Mathf.Abs(speed);
		}

	}

	void FixedUpdate(){
		if (Random.Range(1,800) < changeDirChance) {
			speed *= -1;
		}
	}

	void die(){
		Destroy (gameObject);
	}
}
