# README #

Repository for Watchout Witch, a mobile game created in Unity during the Spring 2015 semester at USC.

* Watchout Witch is a sidescrolling shooter where you have to manage magic resources while trying to save your cat!
* Version 1.0

### Setup ###

* Clone the repository
* Open the project in Unity (note this project was made in Unity v4.6)
* Build for iOS/Android or just press play to use keyboard/mouse input